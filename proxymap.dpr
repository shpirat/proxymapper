program proxymap;
{$APPTYPE CONSOLE}
uses
  windows, SysUtils, synautil,
  utils in 'includes\utils.pas',
  proxy_class in 'includes\proxy_class.pas';

{*******}

procedure printHelp;
begin
  with u do
  begin
    Writeln('�������������:');
    Writeln('   ' + u.getMyExeName + ' -listen <[addr:]port> -connect <host:port>');
    Writeln('    [-pt ProxyType] [-proxy <[user:pass@]proxyhost:proxyport>]');
    Writeln('');
    Writeln('���������:');
    Writeln('');
    Writeln(' -l -listen <[addr:]port>        ��������� ���� ��� �������� �����������.');
    Writeln('                                 ���� �� ������� addr, �� ���� ����� ');
    Writeln('                                 ��������� �� ���� ������� �����������');
    Writeln('');
    Writeln(' -c -connect <host:port>         ����� � ���� ��������� �������, �����������');
    Writeln('                                 � �������� ����� ���������� �� ��������� ����.');
    Writeln('                                 ��� ������, ��� ����� ��������� � ����������');
    Writeln('                                 ����� ����� ��������� ���� � ��������� ');
    Writeln('                                 ������� � �����');
    Writeln('');
    Writeln(' -pt -proxy_type <ProxyType>     ������� ��� ������������� ������.');
    Writeln('                                 ��������� ��������: HTTP, Socks4, Socks5.');
    Writeln('                                 ��-��������� ������������: HTTP.');
    Writeln('');
    Writeln(' -p -proxy <[user@]server>       ������������ ������-������. ����� ������ �');
    Writeln('                                 ������ ��� ����������� (���� ��� �����)');
    Writeln('                                 ����������� � ����� �������:');
    Writeln('                                             user:pass@server:port');
  end;
end;

var
  proxyMapper: TProxyMapper;

procedure parseParams;
var
  laddr: string;
begin
  if (u.paramExists('-h') or u.paramExists('-help')) then
  begin
    printHelp;
    halt;
  end;

  if (u.paramExists('-l') or u.paramExists('-listen')) then
  begin
    laddr := u.getLastParamValue;
    if (Pos(':', laddr) <> 0) then
    begin
      proxyMapper.ListenAddr := SeparateLeft(laddr, ':');
      proxyMapper.ListenPort := SeparateRight(laddr, ':');
    end else
      proxyMapper.ListenPort := u.getLastParamValue;
  end;

  if (u.paramExists('c') or u.paramExists('-connect')) then
  begin
    proxyMapper.ConnectHost := SeparateLeft(u.getLastParamValue, ':');
    proxyMapper.ConnectPort := SeparateRight(u.getLastParamValue, ':');
  end;

  if (u.paramExists('-p') or u.paramExists('-proxy')) then
  begin
    proxyMapper.ProxyParams :=
      proxyMapper.getProxyParamsFromStr(u.getLastParamValue);
    //��� ������� ������, ������ ����� ��� ������������!
    proxyMapper.ProxyType := ptHTTP;
  end;

  if (u.paramExists('-pt') or u.paramExists('-proxy_type')) then
    proxyMapper.ProxyType :=
      proxyMapper.getProxyTypeFromStr(u.getLastParamValue);
end;

function checkParams: boolean;
begin
  if (proxyMapper.ListenPort = '') then
    u.WriteError('���������� ������� ��������� ��������� ����');
  if (proxyMapper.ConnectHost = '') then
    u.WriteError('���������� ������ ����� ��������� �������');
  if (proxyMapper.ConnectPort = '') then
    u.WriteError('���������� ������� ���� ��������� �������');
  if (proxyMapper.ProxyType <> ptNone) then
  begin
    if (proxyMapper.ProxyParams.Host = '') then
      u.WriteError('�� ������ ����� ������-�������');
    if (proxyMapper.ProxyParams.Port = '') then
      u.WriteError('�� ������ ���� ������-�������');
  end;
  Result := not u.ErrorExists;
end;

procedure tryToConnect;
begin
  with proxyMapper do
  begin
    u.Writeln('LocalPort: ' + ListenPort + ', Connect to ' + ConnectHost + ':'
      + ConnectPort);
    if (ProxyType <> ptNone) then
    begin
      u.Write('Using ' + getProxyStringFromType(ProxyType) +
        ' proxy. Addr: ' + ProxyParams.Host + ':' + ProxyParams.Port);
      if (ProxyParams.User <> '') then
        writeln(', User: ' + proxyParams.user)
      else
        Writeln('');
    end;

    Resume; //resume proxyMapper thread!
  end;
end;

begin
  Writeln('Proxy Mapper');
  if (ParamCount = 0) then
  begin
    printHelp;
    exit;
  end;

  proxyMapper := TProxyMapper.create;

  parseParams;
  if (not checkParams) then
  begin
    exit;
  end;

  tryToConnect;

  u.WriteInfo('Press [Enter] to terminate.');
  Readln;
  u.WriteInfo('Closing connections and threads...');
  proxyMapper.Terminate;
end.

