unit proxy_class;

interface

uses
  Classes, SysUtils, windows, StrUtils,
  blcksock, synsock, utils, synautil;

type
  TProxyMapper = class;
  TClientThread = class;

  TProxyType = (ptNone, ptHTTP, ptSocks4, ptSocks5);

  TProxyParams = record
    Host, Port, User, Pass: string;
  end;

  TClientThread = class(TThread)
  private
    _pm: TProxyMapper;
    _sock: TTCPBlockSocket;
    _connectSock: TTCPBlockSocket;
  public
    function RelayTCP(const fsock, dsock: TTCPBlockSocket): boolean;
    constructor create(client_socket: integer; pm: TProxyMapper);
    procedure execute; override;
  end;

  TProxyMapper = class(TThread)
  private
    _listenSock: TTCPBlockSocket;
    FTimeout: integer;
    FListenPort: string;
    FConnectPort: string;
    FConnectHost: string;
    FProxyType: TProxyType;
    FProxyParams: TProxyParams;
    FListenAddr: string;
    procedure SetTimeout(const Value: integer);
    procedure SetConnectHost(const Value: string);
    procedure SetConnectPort(const Value: string);
    procedure SetListenPort(const Value: string);
    procedure SetProxyType(const Value: TProxyType);
    procedure SetProxyParams(const Value: TProxyParams);
    procedure SetListenAddr(const Value: string);
  public
    constructor create;
    function isTerminated: Boolean;
    procedure execute; override;
    function getProxyParamsFromStr(str: string): TProxyParams;
    function getProxyTypeFromStr(prString: string): TProxyType;
    function getProxyStringFromType(proxyType: TProxyType): string;
  published
    property ListenPort: string read FListenPort write SetListenPort;
    property ListenAddr: string read FListenAddr write SetListenAddr;
    property ConnectPort: string read FConnectPort write SetConnectPort;
    property ConnectHost: string read FConnectHost write SetConnectHost;
    property ProxyType: TProxyType read FProxyType write SetProxyType;
    property ProxyParams: TProxyParams read FProxyParams write SetProxyParams;
    property Timeout: integer read FTimeout write SetTimeout;
  end;

implementation

{ TProxyMapper }

constructor TProxyMapper.create;
begin
  _listenSock := TTCPBlockSocket.Create;
  FProxyType := ptNone;
  FListenPort := '';
  FListenAddr := '0.0.0.0';
  FConnectPort := '';
  FConnectHost := '';
  FreeOnTerminate := True;
  FTimeout := 12000;
  inherited create(true);
end;

function TProxyMapper.getProxyParamsFromStr(str: string): TProxyParams;
var
  s: string;
begin
  if (Pos('@', str) <> 0) then
  begin
    s := SeparateLeft(str, '@');
    result.User := SeparateLeft(s, ':');
    result.Pass := SeparateRight(s, ':');
    s := SeparateRight(str, '@');
  end
  else
    s := str;
  result.Host := SeparateLeft(s, ':');
  result.Port := SeparateRight(s, ':');
end;

function TProxyMapper.getProxyTypeFromStr(prString: string): TProxyType;
begin
  prString := LowerCase(prString);
  result := ptHTTP;
  if (prString = 'socks4') then
    result := ptSocks4;
  if (prString = 'socks5') then
    result := ptSocks5;
end;

procedure TProxyMapper.execute;
var
  client_socket: integer;
begin
  inherited;
  _listenSock.bind(FListenAddr, FListenPort);
  if _listenSock.LastError <> 0 then
  begin
    u.WriteError('Bind local port ' + FListenPort + ' failed! Reason: ' +
      _listenSock.LastErrorDesc);
    halt(0);
    Exit;
  end;
  _listenSock.setLinger(true, 10000);
  _listenSock.listen;
  u.WriteInfo('Listening on  ' + FListenAddr + ':' + FListenPort);
  repeat
    if terminated then
      break;

    if _listenSock.canread(3000) then
    begin
      if _listenSock.lastError = 0 then
      begin
        client_socket := _listenSock.Accept;
        if (client_socket <> -1) then
          TClientThread.Create(client_socket, self);
      end;

    end;
  until false;
  _listenSock.free;
end;

function TProxyMapper.isTerminated: Boolean;
begin
  result := Terminated;
end;

procedure TProxyMapper.SetConnectHost(const Value: string);
begin
  FConnectHost := Value;
end;

procedure TProxyMapper.SetConnectPort(const Value: string);
begin
  FConnectPort := Value;
end;

procedure TProxyMapper.SetListenPort(const Value: string);
begin
  FListenPort := Value;
end;

procedure TProxyMapper.SetProxyType(const Value: TProxyType);
begin
  FProxyType := Value;
end;

procedure TProxyMapper.SetTimeout(const Value: integer);
begin
  FTimeout := Value;
end;

procedure TProxyMapper.SetProxyParams(const Value: TProxyParams);
begin
  FProxyParams := Value;
end;

function TProxyMapper.getProxyStringFromType(
  proxyType: TProxyType): string;
begin
  case proxyType of
    ptNone: result := 'NONE';
    ptHTTP: result := 'HTTP';
    ptSocks4: result := 'Socks4';
    ptSocks5: Result := 'Socks5';
  end;
end;

procedure TProxyMapper.SetListenAddr(const Value: string);
begin
  FListenAddr := Value;
end;

{ TClientThread }

constructor TClientThread.create(client_socket: Integer;
  pm: TProxyMapper);
begin
  _sock := TTCPBlockSocket.Create;
  _sock.Socket := client_socket;
  _pm := pm;
  FreeOnTerminate := true;
  inherited create(false);

  u.WriteInfo('Client accepted! RemoteAddr: ' + _sock.GetRemoteSinIP + ':' +
    IntToStr(_sock.GetRemoteSinPort));
end;

procedure TClientThread.execute;
begin
  inherited;
  _connectSock := TTCPBlockSocket.Create;

  case (_pm.ProxyType) of
    ptNone: ;

    ptHTTP:
      begin
        _connectSock.HTTPTunnelIP := _pm.ProxyParams.Host;
        _connectSock.HTTPTunnelPort := _pm.ProxyParams.Port;
        _connectSock.HTTPTunnelUser := _pm.ProxyParams.User;
        _connectSock.HTTPTunnelPass := _pm.ProxyParams.Pass;
      end;

    ptSocks4, ptSocks5:
      begin
        _connectSock.SocksIP := _pm.ProxyParams.Host;
        _connectSock.SocksPort := _pm.ProxyParams.Port;
        _connectSock.SocksUsername := _pm.ProxyParams.User;
        _connectSock.SocksPassword := _pm.ProxyParams.Pass;
        _connectSock.SocksResolver := True;

        if (_pm.ProxyType = ptSocks4) then
          _connectSock.SocksType := ST_Socks4
        else
          _connectSock.SocksType := ST_Socks5;
      end;
  end;

  _connectSock.Connect(_pm.ConnectHost, _pm.ConnectPort);
  if (_connectSock.LastError <> 0) then
  begin
    u.WriteError('Connect to ' + _pm.ConnectHost + ':' + _pm.ConnectPort +
      ' failed. Reason: ' + _connectSock.LastErrorDesc);

    _sock.CloseSocket;
    exit;
  end;

  RelayTCP(_connectSock, _sock);
  //u.WriteInfo('Relay thread terminated.');
end;

function TClientThread.RelayTCP(const fsock,
  dsock: TTCPBlockSocket): boolean;
var
  n: integer;
  buf: string;
  ql, rl: TList;
  fgsock, dgsock: TTCPBlockSocket;
  FDSet: TFDSet;
  FDSetSave: TFDSet;
  TimeVal: PTimeVal;
  TimeV: TTimeVal;
begin
  result := false;
  //buffer maybe contains some pre-readed datas...
  if fsock.LineBuffer <> '' then
  begin
    buf := fsock.RecvPacket(_pm.timeout);
    if fsock.LastError <> 0 then
      Exit;
    dsock.SendString(buf);
  end;
  //begin relaying of TCP
  ql := TList.Create;
  rl := Tlist.create;
  try
    TimeV.tv_usec := (_pm.Timeout mod 1000) * 1000;
    TimeV.tv_sec := _pm.Timeout div 1000;
    TimeVal := @TimeV;
    if _pm.Timeout = -1 then
      TimeVal := nil;
    FD_ZERO(FDSetSave);
    FD_SET(fsock.Socket, FDSetSave);
    FD_SET(dsock.Socket, FDSetSave);
    FDSet := FDSetSave;
    while synsock.Select(65535, @FDSet, nil, nil, TimeVal) > 0 do
    begin
      rl.clear;
      if FD_ISSET(fsock.Socket, FDSet) then
        rl.Add(fsock);
      if FD_ISSET(dsock.Socket, FDSet) then
        rl.Add(dsock);
      for n := 0 to rl.Count - 1 do
      begin
        fgsock := TTCPBlockSocket(rl[n]);
        if fgsock = fsock then
          dgsock := dsock
        else
          dgsock := fsock;
        if fgsock.WaitingData > 0 then
        begin
          buf := fgsock.RecvPacket(0);
          dgsock.SendString(buf);
          if dgsock.LastError <> 0 then
            exit;
        end
        else
          exit;
      end;
      FDSet := FDSetSave;
    end;
  finally
    rl.free;
    ql.free;
  end;
  result := true;
end;

end.

